function s = getArmijo(W, g, beta, sigma, imgs, y)
    s = 1;
    fx = myCostFunc({W{1},W{3}},{W{2},W{4}},imgs,y);
    gradF_vec = [];
    for i = 1:4
        gradF_vec = [gradF_vec; g{i}(:)];        
    end
    gradF_vec_norm = -gradF_vec'*gradF_vec;
    
    while true
        W_cost = {W{1}-s*g{1},W{3}-s*g{3}};
        b_cost = {W{2}-s*g{2},W{4}-s*g{4}};
        lhs = myCostFunc(W_cost,b_cost,imgs, y);
        rhs = fx + sigma*s*gradF_vec_norm;
        if lhs <= rhs
            break
        elseif s == 0
            error('armijo done goofed')
        end
        s = s*beta;
    end    
end