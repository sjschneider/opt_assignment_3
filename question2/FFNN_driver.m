%This is the driver I have setup for our FFNN
clear all 
clc

% add the data to our path
addpath([pwd strrep('/data','/',filesep)])


%we now load the images a labels

images = loadMNISTImages('fashion_train-images-idx3-ubyte');
labels = loadMNISTLabels('fashion_train-labels-idx1-ubyte');

%setup the dimensions for this problem.
nx = 784;
dimW1 = [128,784];
nb1 = 128;

dimW2 = [10,128];
nb2 = 10;

%initialise values for our 2 weight matricies.
% we do not initalise these as zero as this forces all weights to be the
% same later on.
W{1} = randn(dimW1)*0.3;
b{1} = randn(nb1,1)*0.3;
W{2} = randn(dimW2)*0.3;
b{2} = randn(nb2,1)*0.3;


%Recast the labels to be a matrix with the columns being a probability
%vector with a single entry equal to 1, corresponding to the correct label.
y_actual = recast_labels(labels);
labels2 = labels +1;


alpha = 0.01;

maxit = 500000;
tic
[W_after_SGD,b_after_SGD,F,accuracySGD] = mySGD(W, b, y_actual, images, alpha, maxit,labels2);
disp('SGD time')
toc

tic
[W_after_NestSGD,b_after_NestSGD,F_nest,accuracyNSGD] = myNestSGD(W, b, y_actual, images, alpha, maxit,labels2);
disp('Nesterov SGD time')
toc

tic
alpha = 10^(-4);
beta1 = 0.9;
beta2 = 0.99;
[W_after_ADAM,b_after_ADAM,F_adam,accuracyADAM] = myADAM(W, b, y_actual, images, alpha, maxit, beta1, beta2,labels2);
disp('ADAM time')
toc


t = 1:10000:maxit+1;
figure(1)
plot(t,F,'b')
title('F vs iteration for SGD')
xlabel('iteration')
ylabel('F')

figure(2)
plot(t,accuracySGD,'b')
title('Classification accuracy (%) vs iteration for SGD')
xlabel('iteration')
ylabel('Classification accuracy (%)')

figure(3)
plot(t,F,'b')
hold on
plot(t,F_nest,'r')
plot(t,F_adam,'k')
title('F vs iteration')
xlabel('iteration')
ylabel('F')
legend('SGD','Nesterov SGD','ADAM')

figure(4)
plot(t,accuracySGD,'b')
hold on
plot(t,accuracyNSGD,'r')
plot(t,accuracyADAM,'k')
title('Classification accuracy (%) vs iteration')
xlabel('iteration')
ylabel('Classification accuracy (%)')
legend('SGD','Nesterov SGD','ADAM')


% SGD Comparison
for i = 1:size(images,2)
    x = images(:,i);
%hidden layer
z{1} = W_after_SGD{1}*x + b_after_SGD{1};
h{1} = myReLU(z{1});

%output layer
z{2} = W_after_SGD{2}*h{1} + b_after_SGD{2};
y_pred = mysoftmax(z{2});

[~,y_ML_after_SGD(i,1)] = max(y_pred);
end
C = (y_ML_after_SGD == labels2);
SGD_percent_correct = sum(y_ML_after_SGD == labels2 )/size(images,2)*100


% Nestorov Comparison
for i = 1:size(images,2)
    x = images(:,i);

%hidden layer
z{1} = W_after_NestSGD{1}*x + b_after_NestSGD{1};
h{1} = myReLU(z{1});

%output layer
z{2} = W_after_NestSGD{2}*h{1} + b_after_NestSGD{2};
y_pred = mysoftmax(z{2});

[~,y_ML_after_NestSGD(i,1)] = max(y_pred);
end
C = (y_ML_after_NestSGD  == labels2);
Nesterov_NestSGD_percent_correct = sum(y_ML_after_NestSGD == labels2 )/size(images,2)*100


% ADAM Comparison
for i = 1:size(images,2)
    x = images(:,i);

%hidden layer
z{1} = W_after_ADAM{1}*x + b_after_ADAM{1};
h{1} = myReLU(z{1});

%output layer
z{2} = W_after_ADAM{2}*h{1} + b_after_ADAM{2};
y_pred = mysoftmax(z{2});

[~,y_ML_after_ADAM(i,1)] = max(y_pred);
end
C = (y_ML_after_ADAM  == labels2);
ADAM_percent_correct = sum(y_ML_after_ADAM == labels2 )/size(images,2)*100


for i = 1:size(images,2)
    x = images(:,i);
    %hidden layer
    z{1} = W{1}*x + b{1};
    h{1} = myReLU(z{1});

    %output layer
    z{2} = W{2}*h{1} + b{2};
    y_pred = mysoftmax(z{2});

    [~,y_ML(i,1)] = max(y_pred);
end

sum(y_ML == labels2 )/size(images,2)




