function [W,b,F,accuracy] = myGD(W0, b0, y_actual, images,maxit,step, labels2)
% Train a two-layer neural network using full gradient descent
% step: 'const' - constant step size at every iteration
%       'arm'   - armijo defined step size at every iteration

% Initialise variables
N = size(images,2);
beta = 1/2;
sigma = 1/2;
Ws = cell(1,4); % W1, b1, W2, b2
Ws{1} = W0{1};
Ws{3} = W0{2};
Ws{2} = b0{1};
Ws{4} = b0{2};
gs = cell(1,4);
up = cell(1,4);

k = 0;
while k <= maxit
    F(k+1) = myCostFunc({Ws{1},Ws{3}}, {Ws{2},Ws{4}}, images, y_actual);    
   for i = 1:size(images,2)
        x = images(:,i);
        %hidden layer
        z{1} = Ws{1}*x + Ws{2};
        h{1} = myReLU(z{1});

        %output layer
        z{2} = Ws{3}*h{1} + Ws{4};
        y_pred = mysoftmax(z{2});

        [~,y_ML(i,1)] = max(y_pred);
    end
    accuracy(k+1) = sum(y_ML == labels2 )/size(images,2);    
    
    % Initialise gradients
    for j = 1:4
        gs{j} = zeros(size(Ws{j}));
    end
    
    % Cacluate full gradient
    for i = 1:size(images,2)
        x = images(:,i);
        y = y_actual(:,i);
        [up{1}, up{2}, up{3}, up{4}] = grad_w_f({Ws{1},Ws{3}},{Ws{2},Ws{4}},x,y);
        for j = 1:4
            gs{j} = gs{j} + up{j}./size(images,2);
        end
    end
    
    % Get step length
    if strcmp(step,'const')
        alpha = 10^-4;
    elseif strcmp(step,'arm')
        alpha = getArmijo(Ws, gs, beta, sigma, images, y_actual);
    end

    % Update weights
    for j = 1:4
        Ws{j} = Ws{j} - alpha * gs{j};        
    end
    k = k + 1;
end
W = {Ws{1}, Ws{3}};
b = {Ws{2}, Ws{4}};