function [W,b,F, accuracy] = myADAM(W0, b0, y_actual, images, alpha,maxit, beta1, beta2,labels2)
% Train a two-layer neural network using the ADAM optimisation method

% Initialise variables
N = size(images,2);
eps = 10^-8;

Ws = cell(1,4); % W1, b1, W2, b2
Ws{1} = W0{1};
Ws{3} = W0{2};
Ws{2} = b0{1};
Ws{4} = b0{2};

gs = cell(1, 4);
ms = {0,0,0,0};
vs = {0,0,0,0};

k = 0;
while k <= maxit
    % Calculate cost and classification values for convergence
    if mod(k,10000) == 0
        ix = floor(k/10000) + 1;
        F(ix) = myCostFunc({Ws{1},Ws{3}}, {Ws{2},Ws{4}}, images, y_actual);
        for i = 1:size(images,2)
            x = images(:,i);
            %hidden layer
            z{1} = Ws{1}*x + Ws{2};
            h{1} = myReLU(z{1});
            
            %output layer
            z{2} = Ws{3}*h{1} + Ws{4};
            y_pred = mysoftmax(z{2});
            
            [~,y_ML(i,1)] = max(y_pred);
        end
        accuracy(ix) = sum(y_ML == labels2 )/size(images,2);
    end    
    
    % Sample image and calculate gradient
    i = randi([1 N]);
    x = images(:,i);
    y = y_actual(:,i);
    [gs{1}, gs{2}, gs{3}, gs{4}] = grad_w_f({Ws{1},Ws{3}},{Ws{2},Ws{4}},x,y);

    % Update weights
    for j = 1:4
        ms{j} = beta1*ms{j} + (1-beta1)*gs{j};
        vs{j} = beta2*vs{j} + (1-beta2)*(gs{j}.^2);
        m_hat = ms{j}/(1-beta1^(k+1));
        v_hat = vs{j}/(1-beta2^(k+1));
        Ws{j} = Ws{j} - alpha * m_hat./(sqrt(v_hat)+eps);        
    end
    k = k + 1;
end
W = {Ws{1}, Ws{3}};
b = {Ws{2}, Ws{4}};