function [W,b,F,accuracy] = myNestSGD(W0, b0, y_actual, images, alpha,maxit,labels2)
%my_NestSGD performs stochastic gradient descent with Nesterov acceleration to solve the problem of image
%recognition for the specific FFNN created in part a) of question 2.
%INPUT:
%   W0: initial guess for W weights.
%   b0: initial guess for b weights
%   y_actual: the label data vectorised as a probability vecotr for each image
%   where each image contains a probability 1 for the type of clothing it is
%   and 0 elsewhere.
%   images: the image data
%   alpha: the step size for SGD
%   maxit: the maximum number of iterations to be performed
%   labels2: image labels ranging from 1-10
%   setup N to sample randomly over the input images.
%OUTPUT
%   W: The final weights W calculated
%   b: the final weight b calculated
%   F: the cost calculated every 10,000 iterations
%   accuacy: the accuracy calculated every 10,000 iterations
N = size(images,2);

W = W0;
b = b0;
k = 1;

%setup the first gradient sample
%choose i randomly from the 60,000 images to calculate the gradient, we
%sample with replacement.
i = randi([1 N]);
x = images(:,i);
y = y_actual(:,i);
[grad_W1_f, grad_b1_f, grad_W2_f, grad_b2_f] = grad_w_f(W,b,x,y);

cnt = 1;
%calculate the cost.
F(cnt) = myCostFunc(W, b, images, y_actual);

%setup Nesterov momentum step size
beta = 0.5;
while k <= maxit
    %step directions for each set of parameters.
    dW1 = grad_W1_f;
    db1 = grad_b1_f;
    dW2 = grad_W2_f;
    db2 = grad_b2_f;
    
    if k == 1
        %regular SGD
        W{1} = W{1} - alpha * dW1;
        V_W1_old = W{1};
        b{1} = b{1} - alpha * db1;
        V_b1_old = b{1};
        W{2} = W{2} - alpha * dW2;
        V_W2_old = W{2};
        b{2} = b{2} - alpha * db2;
        V_b2_old = b{2};
    else
        %Nesterov-SGD
        V_W1_new = W{1} - alpha * dW1;
        W{1} = V_W1_new + beta*(V_W1_new - V_W1_old);
        V_W1_old = V_W1_new;
        
        V_b1_new = b{1} - alpha * db1;
        b{1} = V_b1_new + beta*(V_b1_new - V_b1_old);
        V_b1_old = V_b1_new;
        
        V_W2_new = W{2} - alpha * dW2;
        W{2} = V_W2_new + beta*(V_W2_new - V_W2_old);
        V_W2_old = V_W2_new;
        
        V_b2_new = b{2} - alpha * db2;
        b{2} = V_b2_new + beta*(V_b2_new  - V_b2_old);
        V_b2_old  = V_b2_new ;
    end
   
    %random choice of image and gradient calc
    i = randi([1 N]);
    x = images(:,i);
    y = y_actual(:,i);
    [grad_W1_f, grad_b1_f, grad_W2_f, grad_b2_f] = grad_w_f(W,b,x,y);
    
    %Calculation of cost F and accuracy
    if mod(k,10000) == 0
        cnt = cnt +1;
        F(cnt,1) = myCostFunc(W, b, images, y_actual);
        for i = 1:size(images,2)
            x = images(:,i);
            %hidden layer
            z{1} = W{1}*x + b{1};
            h{1} = myReLU(z{1});
            
            %output layer
            z{2} = W{2}*h{1} + b{2};
            y_pred = mysoftmax(z{2});
            
            [~,y_ML(i,1)] = max(y_pred);
        end
        accuracy(cnt) = sum(y_ML == labels2 )/size(images,2);
    end
    
    k = k +1;
end