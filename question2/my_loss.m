function [f,y] = my_loss(W, b, x, y_actual)
%calculates the loss using the 2-norm^2 difference between the probability
%label vectors.
n = size(x,1);
f = zeros(n,1);
for i = 1:n
    %evaluate the FFNN
    %hidden layer
    z{1} = W{1}*x(:,i) + b{1};
    h{1} = myReLU(z{1});
    
    %output layer
    z{2} = W{2}*h{1} + b{2};
    y(:,i) = mysoftmax(z{2});
    
    
    %compare the prediction probability to the actual probability.
    f(i,1) = norm(y_actual(:,i) - y(:,i))^2;
end