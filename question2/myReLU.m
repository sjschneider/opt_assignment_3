function h = myReLU(z)
%returns h as the ReLU given input vector z.
h = max(0, z);