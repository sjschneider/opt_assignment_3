function y = recast_labels(labels)
%here we recast the label vector to be a matrix of probabilites for each
%label, where the actuall label is given probability 1 and the others are
%given a value of 0.

y = zeros(10, length(labels));

for i = 1:length(labels)
    y(labels(i)+1,i) = 1;
end