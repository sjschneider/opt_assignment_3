function y = mysoftmax(z)
%here we calculate the value for y as the softmax of each element in z
y = exp(z)./sum(exp(z));