function cost_F = myCostFunc(W, b, x, y_actual)
%calculates the cost of our function F using the loss function my_loss.
%we then average over the losses.
f = my_loss(W,b, x, y_actual);   
cost_F = 1/length(f) * sum(f);