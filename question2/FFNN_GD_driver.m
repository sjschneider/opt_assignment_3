%Driver code for plotting the outputs of the gradient descent algorithms
clear all 
clc

% add the data to our path
addpath([pwd strrep('/data','/',filesep)])

%we now load the images a labels
images = loadMNISTImages('fashion_train-images-idx3-ubyte');
labels = loadMNISTLabels('fashion_train-labels-idx1-ubyte');

%setup the dimensions for this problem.
nx = 784;
dimW1 = [128,784];
nb1 = 128;

dimW2 = [10,128];
nb2 = 10;

%initialise values for our 2 weight matricies.
% we do not initalise these as zero as this forces all weights to be the
% same later on.
W{1} = randn(dimW1)*0.3;
b{1} = randn(nb1,1)*0.3;
W{2} = randn(dimW2)*0.3;
b{2} = randn(nb2,1)*0.3;


%Recast the labels to be a matrix with the columns being a probability
%vector with a single entry equal to 1, corresponding to the correct label.
y_actual = recast_labels(labels);
labels2 = labels +1;


maxit_gd = 20;
tic
[W_after_GD,b_after_GD,F_gd,acc_gd] = myGD(W, b, y_actual, images, maxit_gd, 'const', labels2);
disp('GD with constant step time')
toc

tic
[W_after_GD_arm,b_after_GD_arm,F_gd_arm,acc_gd_arm] = myGD(W, b, y_actual, images, maxit_gd, 'arm', labels2);
disp('GD with armijo step time')
toc

t = 1:maxit_gd+1;
figure
plot(t,F_gd,'b')
hold on
plot(t,F_gd_arm,'r')
title('F vs iteration for GD')
xlabel('Iteration')
ylabel('F')
legend('constant', 'armijo')

figure
plot(t,acc_gd,'b')
hold on
plot(t,acc_gd_arm,'r')
title('Classification accuracy (%) vs iteration')
xlabel('Iteration')
ylabel('Classification accuracy (%)')
legend('constant','armijo')

% GD Comparison
for i = 1:size(images,2)
    x = images(:,i);

%hidden layer
z{1} = W_after_GD{1}*x + b_after_GD{1};
h{1} = myReLU(z{1});

%output layer
z{2} = W_after_GD{2}*h{1} + b_after_GD{2};
y_pred = mysoftmax(z{2});

[~,y_ML_after_GD(i,1)] = max(y_pred);
end
C = (y_ML_after_GD  == labels2);
GD_percent_correct = sum(y_ML_after_GD == labels2 )/size(images,2)*100


% GD Comparison
for i = 1:size(images,2)
    x = images(:,i);

%hidden layer
z{1} = W_after_GD_arm{1}*x + b_after_GD_arm{1};
h{1} = myReLU(z{1});

%output layer
z{2} = W_after_GD_arm{2}*h{1} + b_after_GD_arm{2};
y_pred = mysoftmax(z{2});

[~,y_ML_after_GD_arm(i,1)] = max(y_pred);
end
C = (y_ML_after_GD_arm  == labels2);
GD_arm_percent_correct = sum(y_ML_after_GD_arm == labels2 )/size(images,2)*100


for i = 1:size(images,2)
    x = images(:,i);
    %hidden layer
    z{1} = W{1}*x + b{1};
    h{1} = myReLU(z{1});

    %output layer
    z{2} = W{2}*h{1} + b{2};
    y_pred = mysoftmax(z{2});

    [~,y_ML(i,1)] = max(y_pred);
end

sum(y_ML == labels2 )/size(images,2)




