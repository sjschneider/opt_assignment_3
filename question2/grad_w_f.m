function [grad_W1_f, grad_b1_f, grad_W2_f, grad_b2_f] = grad_w_f(W,b,x_i,y_i)
%compute the gradients for image i using back propogation.
%hidden layer
z{1} = W{1}*x_i + b{1};
h{1} = myReLU(z{1});
%output layer
z{2} = W{2}*h{1} + b{2};
y_pred = mysoftmax(z{2});

%calculate grad_z2_f
for i = 1:10
    for k = 1:10
        if i ==k
            dy_dzJ(i,k) = y_pred(i) - y_pred(k)^2;
        else
            dy_dzJ(i,k) = -y_pred(i) * y_pred(k);
        end
    end
end
grad_ypred_f = -2*(y_i - y_pred);

grad_z2_f = dy_dzJ' * grad_ypred_f;

%calculate grad_W2_f
grad_W2_f = grad_z2_f * h{1}';

%calculate grad_b2_f
grad_b2_f = grad_z2_f;

%calculate grad_z1_f
%we first caculate the jacobian of h1 with respect to z1
grad_z1_f = eye(size(z{1},1));
for i = 1:size(z{1},1)
    if z{1}(i) <0
        grad_z1_f(i,i) =  0;
    end
end
grad_z1_f = grad_z1_f * W{2}'*(grad_z2_f);

%calculate grad_W1_f
grad_W1_f = grad_z1_f * x_i';

%calculate grad_b1_f
grad_b1_f = grad_z1_f;